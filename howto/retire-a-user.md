"Retiring" a user can actually mean two things:

 * "retired", which disables their access to Tor hosts but keeps
   email working and then automatically stops after 186 days

 * "disabled", which immediately disables everything

# How to retire a user

This is done by "locking" the account in [howto/ldap](howto/ldap), so it should simply
be:

    ssh db.torproject.org ud-lock account

Note that it's unclear if we should add an email alias in the
`virtual` file when the account expires, see [ticket #32558](https://bugs.torproject.org/32558) for
details.

# How to un-retire a user

To reverse the above, you need to restore those LDAP fields the way
they were before. You can only do this by restoring from the [howto/LDAP](howto/LDAP)
database. No, that is not fun at all. Be careful to avoid duplicate
fields when you re-add them in ldapvi.

If the user was just "locked", you might be able to re-enable it by
doing the following:

 * delete the `accountStatus`, `shadowExpire` fields
 * add the `keyFingerprint` field matching the (trusted) fingerprint
 * change the user's password to something that is not locked

To set a password, you need to find a way to generate a salted UNIX
hashed password, and there are many ways to do that, but if you have a
copy of the userdir-ldap source code lying around, this could just do
it:

    >>> from userdir_ldap import HashPass, GenPass
    >>> print("{crypt}" + HashPass(GenPass()))

# How to disable a user

This is done by removing all traces of the account:

 1. Login to alberti.torproject.org and lock the LDAP account using `ud-info -u`
 2. Login as admin to gitlab.torproject.org and disable the user account.
 3. Login to eugeni.torproject.org.
    - edit `/etc/postfix/virtual` and remove the account alias.
    - run `sudo postmap virtual` to rebuild the virtual users table.
    - run `sudo remove_members <list names> <email address>`
 4. make sure they don't have keys and accounts in Puppet
 5. remove the key from `acccount-keyring.git`

There are other manual accounts that are *not* handled by LDAP. Make
sure you check:

 * Nextcloud
 * Trac
 * Gitolite
 * Gitlab - remove the user from the config, as a cleanup (disabled
   users already do not have access to gitolite)

TODO: list is incomplete, need to audit [the service list](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service) and see
which services are in LDAP and which aren't. See [ticket #32519](https://bugs.torproject.org/32519).
